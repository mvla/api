<?php

namespace Mvla\Api;


trait ValidationTrait
{
    public function mergeRouteRequests(){
        $route = $this->route() ? $this->route()->parameters() : [];
        $request = $this->request->all();

        return array_merge($route, $request);
    }

    /**
     * @return array
     */
    public function getRulesforAction($rules = []) : array
    {
        $actionMethod = $this->mergeRouteRequests() ? $this->route()->getActionMethod() : '';

        $rules = collect($rules)->get((string)$actionMethod);

        return $rules ? $rules: [];
    }
}