<?php
namespace Mvla\Api;

use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Response;

/**
 * Trait ApiTrait
 * @package Mvla\Api
 */
trait ApiTrait
{
    /**
     * @var int
     */
    protected $pageLimit = 10;

    public function getPageLimit(){
        return $this->pageLimit;
    }

    /**
     * @param null $pageLimit
     * @return int
     */
    public function setPageLimit($pageLimit = null)
    {
        if ($pageLimit){
            $this->pageLimit = (int) $pageLimit;
        } else {
            $this->pageLimit = request()->limit ? (int) request()->limit : (int) $this->pageLimit ;
        }
        return $this->getPageLimit();
    }


    /**
     * Default HTTP Status Code
     * @var int
     */
    protected $statusCode = 200;
    /**
     * Default JSON Options
     * @var int
     */
    protected $json_options = JSON_PRETTY_PRINT;

    /**
     * @return int
     */
    protected function getJsonOptions()
    {
        return $this->json_options;
    }

    /**
     * @param int $json_options
     * @return $this
     */
    protected function setJsonOptions($json_options)
    {
        $this->json_options = $json_options;
        return $this;
    }

    /**
     * @return mixed
     */
    protected function getStatusCode()
    {
        return $this->statusCode;
    }

    /**
     * @param mixed $statusCode
     * @return $this
     */
    protected function setStatusCode($statusCode)
    {
        $this->statusCode = $statusCode;

        return $this;
    }

    /**
     * @param $data
     * @param array $headers
     * @return \Illuminate\Http\JsonResponse
     */
    protected function respond($data, $headers =[])
    {
        return Response::json($data, $this->getStatusCode(), $headers, $this->getJsonOptions());
    }

    /**
     * @param $message
     * @return \Illuminate\Http\JsonResponse
     */
    public function respondWithError($message)
    {
        return $this->setJsonOptions(JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES)
            -> respond([
                'error' => [
                    'message' => $message,
                    'status_code' => $this->getStatusCode(),
                    'documentation_url' => url('/')
                ]
            ]);

    }

    /**
     * @param string $message
     * @return \Illuminate\Http\JsonResponse
     */
    public function respondNotFound($message = 'Not Found')
    {
//        return $this->setStatusCode(404)->respondWithError($message);
        return $this->setStatusCode(200)->respondWithError($message);
    }

    /**
     * @param string $message
     * @return \Illuminate\Http\JsonResponse
     */
    public function respondInternalError($message = 'Internal Error!')
    {
        return $this->setStatusCode(500)->respondWithError($message);
    }


    /**
     * @param string $message
     * @return \Illuminate\Http\JsonResponse
     */
    public function respondInvalidEndpoint($message = 'Invalid Resource Endpoint!')
    {
        return $this->setStatusCode(400)->respondWithError($message);
    }

    /**
     * @param array $message
     * @return \Illuminate\Http\JsonResponse
     */
    public function respondValidationError($message = ['Invalid Request!'])
    {
        $message = collect($message)->map(function($item, $key){
            return array_first($item);
        });
        return $this->setStatusCode(400)->respondWithError($message);
    }

    /**
     * @param LengthAwarePaginator $paginator
     * @param $data
     * @return \Illuminate\Http\JsonResponse
     */
    public function respondWithPaginator(LengthAwarePaginator $paginator, $data){
        $data = array_merge([
            'data' => $data,
            'paginator' => [
                'total_count' => $paginator->total(),
                'total_pages' => $paginator->lastPage(),
                'current_page' => $paginator->currentPage(),
                'limit' => $paginator->perPage(),
                'next_page' => $paginator->nextPageUrl(),
                'previous_page' => $paginator->previousPageUrl()
            ]
        ]);

        return $this->respond($data);
    }


    public function respondRestError(){
        $actionMethod = \Route::currentRouteName();
        $message  = "La acción \"${actionMethod}\" no se completó exitosamente.";

        return $this->setStatusCode(404)->respondWithError($message);
    }

    public function respondRest(){
        $actionMethod = \Route::currentRouteName();
        $message  = "La acción \"${actionMethod}\" se completó exitosamente.";

        $data = array_merge([
            'data' => [
                'message' => $message,
                'status_code' => $this->getStatusCode(),
            ]
        ]);

        return $this->respond($data);
    }



}