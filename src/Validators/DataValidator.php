<?php

namespace Mvla\Api\Validators;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\ValidationException;
use Mvla\Api\ApiTrait;
use Illuminate\Contracts\Validation\Validator;
use Mvla\Api\ValidationTrait;

/**
 * Class Validator
 * @package API\Validators
 */
abstract class DataValidator extends FormRequest
{
    use ApiTrait, ValidationTrait;

    protected function failedValidation(Validator $validator)
    {
        throw new ValidationException($validator, $this->respondValidationError(
            $this->formatErrors($validator)
        ));
    }
    public function validationData()
    {
        return $this->mergeRouteRequests();
    }

    public function authorize()
    {
        return true;
    }

}